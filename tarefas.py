from reportlab.lib.pagesizes import letter

from reportlab.pdfgen import canvas as cv

import tkinter as tk

import tkinter.ttk as ttk

import tkinter.tix as tix

import sqlite3

import datetime

from tkinter.constants import *



class Aplicativo(object):
    def __init__(self, master=None):
        self.master = master
        master.title("Tarefas Diárias")
        master.geometry('600x500')
        master.state('zoomed')
        hello =self.sair

        menubar = tk.Menu(master)
        
        # create a pulldown menu, and add it to the menu bar
        filemenu = tk.Menu(menubar, tearoff=0)
        filemenu.add_command(label="Sair", command=self.sair)
        menubar.add_cascade(label="Arquivo", menu=filemenu)

        # create more pulldown menus
        editmenu = tk.Menu(menubar, tearoff=0)
        editmenu.add_command(label="Gerar Relatório", command=GerarRelatorio)
        menubar.add_cascade(label="Ferramentas", menu=editmenu)

        # display the menu
        master.config(menu=menubar)
        
        widget = ttk.Frame(master)
        widget.pack(expand=1)
        
        ttk.Label(widget, text='DIGITE UMA TAREFA:').pack(pady=10)
        
        self.texto = tix.Text(widget, width=60, height=10)
        self.texto.pack()

        bt_inserir = ttk.Button(widget, text='Inserir', command=self.get)
        bt_inserir.pack(pady=10)

    def sair(self):
        self.master.destroy()
        
    def get(self):
        agora = datetime.datetime.now()
        
        hora = '{}:{}:{}'.format(agora.hour, agora.minute, agora.second)
        data = '{}/{}/{}'.format(agora.day, agora.month, agora.year)
        tarefa = self.texto.get(1.0, END)

        self.salvar(tarefa, hora, data)


    def salvar(self, tarefa, hora, data):
        BancoDeDados(tarefa, hora, data).salvar()



class BancoDeDados(object):
    def __init__(self, tarefa, hora, data):
        self.tarefa = tarefa
        self.hora = hora
        self.data = data
        
    def salvar(self):
        con = sqlite3.connect('tarefas.db')
        cur = con.cursor()

        cur.execute("""CREATE TABLE IF NOT EXISTS tarefas (id INTEGER
PRIMARY KEY AUTOINCREMENT, tarefa TEXT, data TEXT, hora TEXT)""")

        cur.execute('INSERT INTO tarefas VALUES (NULL,?,?,?)',(
            self.tarefa, self.data, self.hora))

        con.commit()
        con.close()


class GerarRelatorio(object):
    def __init__(self):
        con = sqlite3.connect('tarefas.db')
        cur = con.cursor()

        cur.execute("SELECT * from tarefas")
        tarefas = cur.fetchall()

        con.close()

        canvas = cv.Canvas("tarefas.pdf", pagesize=letter)
        canvas.setLineWidth(.3)
        canvas.setFont('Helvetica', 12)
         
        canvas.drawString(30,750,'DESCRIÇÃO DA TAREFA')
        canvas.drawString(400,750,'DATA')
        canvas.drawString(500,750,"HORA")

        coluna = 750
        print('tarefas -->', tarefas)
        for ign,  descricao, data, hora in tarefas:
            coluna -= 30
            canvas.drawString(30,coluna, descricao)
            canvas.drawString(400,coluna, data)
            canvas.drawString(500,coluna, hora )
            print(ign)
            
         
        canvas.save()



    
root = tk.Tk()
Aplicativo(root)
